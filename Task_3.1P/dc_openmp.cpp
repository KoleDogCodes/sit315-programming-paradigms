#include "mpi_wrapper.h"
#include <stdlib.h>
#include <ctime>
#include <fstream>
#include <math.h>

using namespace std;

#define SIZE 800
#define MAX_NUMBER 100

//NOTE: ARRAY[ROW][COLUMN]
int A[SIZE][SIZE];
int B[SIZE][SIZE];
int ABC[SIZE][SIZE];

struct timespec Start, End;

void PopulateMatrixes() {
    srand(0);

    //Populate array a
    for (int i = 0; i < SIZE; i++) {
        for (int z = 0; z < SIZE; z++) {
            A[i][z] = rand() % MAX_NUMBER;
        }
    }

    //Populate array b
    for (int i = 0; i < SIZE; i++) {
        for (int z = 0; z < SIZE; z++) {
            B[i][z] = rand() % MAX_NUMBER;
        }
    }
}

void PrintMatrix(string title, int *matrix, int m, int n) {
    ofstream myfile;
    myfile.open (title + ".txt");
    //cout << "[Processor " << GetProcessId() << "] " << title << endl;

    for (int i = 0; i < m; i++) {
        for (int z = 0; z < n; z++) {
            myfile << *((matrix+i*n) + z) << ",";
        }

        myfile << endl;
    }

    myfile.close();
}

void print_timediff(const char* prefix, const struct timespec& start, const struct timespec& end) {
    double milliseconds = end.tv_nsec >= start.tv_nsec
                        ? (end.tv_nsec - start.tv_nsec) / 1e6 + (end.tv_sec - start.tv_sec) * 1e3
                        : (start.tv_nsec - end.tv_nsec) / 1e6 + (end.tv_sec - start.tv_sec - 1) * 1e3;
    printf("%s: %lf seconds\n", prefix, (milliseconds / 1000));
}

void Calculate(int matrix[][SIZE], int rows){
    int C[rows][SIZE];
    int processId = GetProcessId();

    #pragma omp parallel for
    for (int i = 0; i < rows; i++) {
        for (int z = 0; z < SIZE; z++) {
            //Now calculate 
            int res = 0;

            for (int iA = 0; iA < SIZE; iA++) {
                res += matrix[i][iA] * B[iA][z];
            }

            if (processId == 0) {
                ABC[i][z] = res;
            }
            else {
                C[i][z] = res;
            }
            //printf("[%s] C[%d][%d] = %d\n", GetProcessName().c_str(), i, z, res);
        }    
    }  

    if (processId != 0) {
        int size = sizeof(C) / 4; 
        MPISend(&size, 1, MPI_INT, 0, MPI_COMM_WORLD); //Wait for solved chunks from slave nodes
        MPISend(&C, size, MPI_INT, 0, MPI_COMM_WORLD); //Send chunk back to master node
    }

    //PrintMatrix("Sub C Matrix [" + to_string(GetProcessId()) + "]", *C, rows, SIZE); //Output chunk to file (DEBUG PURPOSES)
}

int GetBlockSize(){
    return SIZE / GetProcessesCount(); //Calculate chunk size based on process count
}

int GetBlockStart(int processId){
    const int blockSize = GetBlockSize();
    return processId > 0 ? processId * blockSize + processId : processId * blockSize; //Calculate start of block based on process id
}

int GetBlockEnd(int processId){
    const int blockStart = GetBlockStart(processId);
    const int blockSize = GetBlockSize();
    return blockStart + blockSize > SIZE ? SIZE : blockStart + blockSize; //Calculate end of block based on process id
}

void MasterRoutine() {
    PopulateMatrixes();

    printf("[%s] Process Count: %d | Chunk Size: ~%d\n", GetProcessName().c_str(), GetProcessesCount(), GetBlockSize());
    PrintMatrix("A Matrix", *A, SIZE, SIZE);
    PrintMatrix("B Matrix", *B, SIZE, SIZE);
    
    MPIBroadcast(&B, SIZE * SIZE, MPI_INT, 0, MPI_COMM_WORLD); //Broadcast Matrix B to all processes

    clock_gettime(CLOCK_MONOTONIC, &Start);

    //Send chunks to various processes
    #pragma omp parallel for
    for (int i = 0; i < GetProcessesCount(); i++) {
        const int blockStart = GetBlockStart(i);
        int blockEnd = GetBlockEnd(i) + 1 >= SIZE ? GetBlockEnd(i): GetBlockEnd(i) + 1; //Add on to block end to copy last row when performing std::copy(...)
        int blockSize = abs(blockEnd - blockStart);

        int SubA[blockSize][SIZE]; 
        copy(&A[blockStart][0], &A[0][0] + blockEnd * SIZE, &SubA[0][0]); //Copy section from matrix A to sub matrix

        if (i == 0){
            Calculate(SubA, blockSize);
            continue;
        }

        int size = sizeof(SubA) / 4;
        MPISend(&size, 1, MPI_INT, i, MPI_COMM_WORLD); //Send chunk to respective slave process
        MPISend(&SubA, size, MPI_INT, i, MPI_COMM_WORLD); //Send chunk to respective slave process

        if (blockEnd >= SIZE) { break; } //Break loop if you cannot make anymore chunks with avaialble processes
    }

    //Wait for chunks
    #pragma omp parallel for
    for (int i = 0; i < GetProcessesCount(); i++) {
        if (i == 0) { continue; } //Skip waiting for master results

        const int blockStart = GetBlockStart(i);
        int blockEnd = GetBlockEnd(i) + 1 >= SIZE ? GetBlockEnd(i): GetBlockEnd(i) + 1; //Add on to block end to copy last row when performing std::copy(...)
        int blockSize = abs(blockEnd - blockStart);

        int SubC[blockSize][SIZE]; 
        int size; 

        MPIRecieve(&size, 1, MPI_INT, i, MPI_COMM_WORLD); //Wait for solved chunks from slave nodes
        MPIRecieve(&SubC, size, MPI_INT, i, MPI_COMM_WORLD); //Wait for solved chunks from slave nodes

        copy(&SubC[0][0], &SubC[0][0] + blockSize * SIZE, &ABC[blockStart][0]); //Copy chunk into matrix ABC

        if (blockEnd >= SIZE) { break; } //Break loop if you cannot make anymore chunks with avaialble processes
    }

    clock_gettime(CLOCK_MONOTONIC, &End);
    
    print_timediff("Matrix C:", Start, End);
    PrintMatrix("C Matrix", *ABC, SIZE, SIZE); //Output matrix ABC (result) to a file
}

void SlaveRoutine() {
    MPIBroadcast(&B, SIZE * SIZE, MPI_INT, 0, MPI_COMM_WORLD); //Recieve Matrix B from process 0 (Master)
    //printf("[%s- %d] Recieved Matrix B\n", GetProcessName().c_str(), GetProcessId());

    //Calcuate size of incoming array
    int processId = GetProcessId();
    const int blockStart = GetBlockStart(processId);
    int blockEnd = GetBlockEnd(processId) + 1 >= SIZE ? GetBlockEnd(processId): GetBlockEnd(processId) + 1; //Add on to block end to copy last row when performing std::copy(...)
    int blockSize = abs((blockEnd - blockStart));

    int SubA[blockSize][SIZE];
    int size; 

    MPIRecieve(&size, 1, MPI_INT, 0, MPI_COMM_WORLD); //Wait for solved chunks from slave nodes
    MPIRecieve(&SubA, size, MPI_INT, 0, MPI_COMM_WORLD); //Wait and recieve chunk of matrix a from process 0
    //printf("[%s- %d] Recieved Matrix Sub A\n", GetProcessName().c_str(), GetProcessId());

    Calculate(SubA, blockSize);
    //printf("[%s- %d] Done...\n", GetProcessName().c_str(), GetProcessId());
}

int main() {
    InitializeMPI();

    int processId = GetProcessId();

    //Validate if each processor will do about ~2 rows each
    double averageChunkSize = round((double) SIZE / (double) GetProcessesCount());
    if ((averageChunkSize < 2 || GetProcessesCount() > SIZE) && processId == 0){
        cout << "[SYSTEM] Attempting to use an invalid amount of processors." << endl;
        return 0;
    }    

    if (processId == 0) {
        MasterRoutine();
    }
    else {
        SlaveRoutine();
    }

    FinalizeMPI();

    return 0;
}