#include <iostream>
#include <fstream>
#include <bits/stdc++.h>
#include <pthread.h> 
#include <vector>
#include "print_utili.h"

using namespace std;
 
struct TrafficSignal {
    int trafficId;
    int carsPassed;
    long timestamp;
};

//Variables
vector<string> signals;
vector<TrafficSignal> results;

void GenerateTrafficData() {
    //Seed random egenrator
    srand(time(NULL));

    ofstream myfile;
    myfile.open("sample_data.txt");

    //Set starting timestamp and max number of traffic signals
    long currentTimestamp = time(NULL);
    const int MAX_ID = 9;

    for (int i = 0; i < 5000; i++) {
        currentTimestamp += 5;

        int id = rand() % MAX_ID;
        string signal = to_string(id) + "," + to_string(rand() % 5) + "," + to_string(currentTimestamp);

        myfile << signal + "\n";
    }

    myfile.close();
}

vector<string> StringSplit(string phrase, string delimiter){
    vector<string> list;
    string s = string(phrase);
    size_t pos = 0;
    string token;
    while ((pos = s.find(delimiter)) != string::npos) {
        token = s.substr(0, pos);
        list.push_back(token);
        s.erase(0, pos + delimiter.length());
    }
    list.push_back(s);
    return list;
}

void LoadData() {
    ifstream myfile;
    myfile.open("sample_data.txt");

    string line;

    while (getline(myfile, line)) {
        signals.push_back(line);
    }

    cout << "[MAIN] " + to_string(signals.size()) + " signals has been loaded." << endl;
}

bool compareSignals(TrafficSignal s1, TrafficSignal s2) { 
    return (s1.carsPassed > s2.carsPassed); 
} 

int main() {
    //GenerateTrafficData();

    //Load file into memory
    LoadData();

    int timestamp = 0;

    //Count
    for (int i = 0; i < signals.size(); i++) {
        string line = signals[i];
        vector<string> spilts = StringSplit(line, ",");
            
        TrafficSignal sig;
        sig.trafficId = atoi(spilts[0].c_str());
        sig.carsPassed = atoi(spilts[1].c_str());
        sig.timestamp = atoi(spilts[2].c_str());

        bool exists = false;

        //Find the corrosponding traffic id from array and update it
        for (int i = 0; i < results.size(); i++) {
            if (sig.trafficId == results[i].trafficId){
                results[i].carsPassed += sig.carsPassed;
                exists = true;
            }
        }
        
        //Add traffic signal to array if it does not exist
        if (exists == false) {
            results.push_back(sig);
        }

        //Sort array
        sort(results.begin(), results.end(), compareSignals);
    }

    cout << endl << "------------ Popular Traffic Hotspots (SEQUENTIAL) ------------" << endl;

    //Print results
    for (int i = 0; i < results.size(); i++) {
        cout << "Traffic ID: " + to_string(results[i].trafficId) + " | Number: " + to_string(results[i].carsPassed) + "\n";
    }

    return 0;
}