#include "mpi_wrapper.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <unordered_map>
#include <ctime> 
#include <CL/cl.h>

using namespace std;

const int K = 30;
const int SIZE = 100;
int ITER = 0;
#define MAX_PERCENT 35
#define MAX_ITERATIONS 500

struct DataPoint {
    int x;
    int y;    
    int cls;
};

struct CentreDataPoint {
    DataPoint point;
    int position;
};

//NOTE: ARRAY[ROW][COLUMN]
DataPoint MATRIX[SIZE * SIZE];
DataPoint SubMatrix[SIZE * SIZE];
vector<DataPoint> Points;
DataPoint KPoints[K];
unordered_map<int, DataPoint[2]> MinMax; 
MPI_Datatype MPI_DATAPOINT;
struct timespec Start, End;

//OPENCL FROM HERE
cl_device_id device_id;
cl_context context;
cl_program program;
cl_kernel kernel; //this is your kernel function
cl_command_queue  queue;
cl_event event = NULL;
cl_mem bufA, bufB, bufC;

int err;

const int max = SIZE;
const int TS = 2;
int numberOfPoints = 0;
//size_t local[2] = { (size_t)TS, (size_t)TS };
size_t local[1] = { (size_t) TS };
size_t global[1]; 

cl_device_id create_device();
void setup_openCL_device_context_queue_kernel(char* filename, char* kernelname);
cl_program build_program(cl_context ctx, cl_device_id dev, const char* filename);
void setup_kernel_memory(int pointsSize);
void copy_kernel_args();
void free_memory();

void free_memory() {
   //free the buffers
   clReleaseMemObject(bufA);
   clReleaseMemObject(bufB);
   clReleaseMemObject(bufC);

    //free opencl objects
   clReleaseKernel(kernel);
   clReleaseCommandQueue(queue);
   clReleaseProgram(program);
   clReleaseContext(context);
}

void copy_kernel_args() {
    clSetKernelArg(kernel, 0, sizeof(int), (void*)&numberOfPoints);
    clSetKernelArg(kernel, 1, sizeof(int), (void*)&K);
    clSetKernelArg(kernel, 2, sizeof(int), (void*)&SIZE);
    clSetKernelArg(kernel, 3, sizeof(cl_mem), (void*)&bufA);
    clSetKernelArg(kernel, 4, sizeof(cl_mem), (void*)&bufB);
    clSetKernelArg(kernel, 5, sizeof(cl_mem), (void*)&bufC);
    if(err < 0) {
      perror("Couldn't create a kernel argument");
      printf("error = %d", err);
      exit(1);
   }
}

void setup_kernel_memory(int pointsSize) {
     bufA = clCreateBuffer(context, CL_MEM_READ_WRITE,  SIZE * SIZE * sizeof(DataPoint), NULL, NULL);
     bufB = clCreateBuffer(context, CL_MEM_READ_ONLY,  K * sizeof(DataPoint), NULL, NULL);
     bufC = clCreateBuffer(context, CL_MEM_READ_ONLY, pointsSize * sizeof(DataPoint), NULL, NULL);

    // Copy matrices to the GPU
    clEnqueueWriteBuffer(queue, bufA, CL_TRUE, 0, SIZE * SIZE * sizeof(DataPoint), &MATRIX[0], 0, NULL, NULL);
    clEnqueueWriteBuffer(queue, bufB, CL_TRUE, 0, K * sizeof(DataPoint), &KPoints[0], 0, NULL, NULL);
    clEnqueueWriteBuffer(queue, bufC, CL_TRUE, 0, pointsSize * sizeof(DataPoint), &Points[0], 0, NULL, NULL);
}

void setup_openCL_device_context_queue_kernel(char* filename, char* kernelname) {
    device_id = create_device();
    cl_int err;
    context = clCreateContext(NULL, 1, &device_id, NULL, NULL, &err);
   if(err < 0) {
      perror("Couldn't create a context");
      exit(1);   
    }

    program = build_program(context, device_id, filename );
    queue = clCreateCommandQueueWithProperties(context, device_id, 0, &err);
    if(err < 0) {
      perror("Couldn't create a command queue");
      exit(1);   
    };

    kernel = clCreateKernel(program, kernelname, &err);
    if(err < 0) {
      perror("Couldn't create a kernel");
      printf("error =%d", err);
      exit(1);
    };

}
cl_program build_program(cl_context ctx, cl_device_id dev, const char* filename) {

   cl_program program;
   FILE *program_handle;
   char *program_buffer, *program_log;
   size_t program_size, log_size;
  

   /* Read program file and place content into buffer */
   program_handle = fopen(filename, "r");
   if(program_handle == NULL) {
      perror("Couldn't find the program file");
      exit(1);
   }
   fseek(program_handle, 0, SEEK_END);
   program_size = ftell(program_handle);
   rewind(program_handle);
   program_buffer = (char*)malloc(program_size + 1);
   program_buffer[program_size] = '\0';
   fread(program_buffer, sizeof(char), program_size, program_handle);
   fclose(program_handle);

   /* Create program from file 

   Creates a program from the source code in the add_numbers.cl file. 
   Specifically, the code reads the file's content into a char array 
   called program_buffer, and then calls clCreateProgramWithSource.
   */
   program = clCreateProgramWithSource(ctx, 1, 
      (const char**)&program_buffer, &program_size, &err);
   if(err < 0) {
      perror("Couldn't create the program");
      exit(1);
   }
   free(program_buffer);

   /* Build program 

   The fourth parameter accepts options that configure the compilation. 
   These are similar to the flags used by gcc. For example, you can 
   define a macro with the option -DMACRO=VALUE and turn off optimization 
   with -cl-opt-disable.
   */
   err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
   if(err < 0) {

      /* Find size of log and print to std output */
      clGetProgramBuildInfo(program, dev, CL_PROGRAM_BUILD_LOG, 
            0, NULL, &log_size);
      program_log = (char*) malloc(log_size + 1);
      program_log[log_size] = '\0';
      clGetProgramBuildInfo(program, dev, CL_PROGRAM_BUILD_LOG, 
            log_size + 1, program_log, NULL);
      printf("%s\n", program_log);
      free(program_log);
      exit(1);
   }

   return program;
}

cl_device_id create_device() {

   cl_platform_id platform;
   cl_device_id dev;
   int err;

   /* Identify a platform */
   err = clGetPlatformIDs(1, &platform, NULL);
   if(err < 0) {
      perror("Couldn't identify a platform");
      exit(1);
   } 

   // Access a device
   // GPU
   err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &dev, NULL);
   if(err == CL_DEVICE_NOT_FOUND) {
      // CPU
      err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_CPU, 1, &dev, NULL);
      //cout << "[SYSTEM] USing cpu \n";
   }
   if(err < 0) {
      perror("Couldn't access any devices");
      exit(1);   
   }

   return dev;
}

/*
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/

void print_timediff(const char* prefix, const struct timespec& start, const struct timespec& end) {
    double milliseconds = end.tv_nsec >= start.tv_nsec
                        ? (end.tv_nsec - start.tv_nsec) / 1e6 + (end.tv_sec - start.tv_sec) * 1e3
                        : (start.tv_nsec - end.tv_nsec) / 1e6 + (end.tv_sec - start.tv_sec - 1) * 1e3;
    printf("%s: %lf seconds\n", prefix, (milliseconds / 1000));
}

void PopulateMatrix() {
    srand(0);
    //Populate array randomly
    for (long i = 0; i < (SIZE * SIZE) * ((float) MAX_PERCENT) / 100; i++){
        DataPoint dp;
        dp.x = rand() % SIZE;
        dp.y = rand() % SIZE;
        dp.cls = -1;

        //Validation check to not add duplicated points
        if (MATRIX[dp.y * SIZE + dp.x].cls != -1) {
            MATRIX[dp.y * SIZE + dp.x] = dp;
            Points.push_back(dp);
        }
        
    }
}

void PrintMatrix(string title) {
    //Open File to dump results
    ofstream myfile;
    myfile.open (title + ".txt");

    for (int i = 0; i < SIZE; i++) {
        for (int z = 0; z < SIZE; z++) {

            //Write current value at MATRIX[i][z] location
            if (MATRIX[i * SIZE + z].cls == 0) {
                myfile << " , ";
            }
            else {
                myfile << MATRIX[i * SIZE + z].cls << ", ";
            }
            
        }

        myfile << endl;
    }

    myfile.close();
}

int PointDistance(DataPoint &from, DataPoint &to) {
    int xDifference = to.x - from.x;
    int yDifference = to.y - from.y;
    return sqrt(pow(xDifference, 2) + pow(yDifference, 2));
}

CentreDataPoint SelectCentrePoint(int k, DataPoint &from, DataPoint &to) {
    DataPoint result;
    result.x = (from.x + to.x) / 2;
    result.y = (from.y + to.y) / 2;

    int minDistance = SIZE, minPosition = 0;

    for (int i = 0; i < Points.size(); i++) {
        int distance = PointDistance(result, Points[i]);

        if (distance < minDistance) {
            minDistance = distance;
            minPosition = i;
        }
    }

    CentreDataPoint centreResult;
    centreResult.point = Points[minPosition];
    centreResult.position = minPosition;

    return centreResult;
}

void SelectK() {
    if (ITER > 0) {
        for (int i = 0; i < K; i++) { 
            MATRIX[KPoints[i].y * SIZE + KPoints[i].x].cls = i + 1;
            
            //Add original k point back into points array
            Points.push_back(KPoints[i]);
        }
    }

    if (MinMax.empty()) {
        //Selecting Inital K Points randomly
        for (int i = 0; i < K; i++) {
            //Set the cluster group in the points array
            int position = rand() % (Points.size() - 1);
            Points[position].cls = i + 1;

            //Update kpoints array with the selected point
            KPoints[i] = Points[position];

            //Update matrix to indicate the selected point as reference point
            MATRIX[KPoints[i].y * SIZE + KPoints[i].x].cls = i + 1;
            
            //Delete selected k point from original points array
            Points.erase(Points.begin() + position);
        }

        return;
    }

    //Update matrix to indicate the selected k point as reference point
    for (int i = 0; i < K; i++) { 
        MATRIX[KPoints[i].y * SIZE + KPoints[i].x].cls = i + 1;
        
        //Add original k point back into points array
        Points.push_back(KPoints[i]);

        //Reset current k point with center point of its cluster
        CentreDataPoint centrePoint = SelectCentrePoint(i, MinMax[i][0], MinMax[i][1]);
        KPoints[i] = centrePoint.point;

        //Update matrix to indicate the selected point as reference point
        MATRIX[KPoints[i].y * SIZE + KPoints[i].x].cls = i + 1;

        //Delete selected k point from original points array
        Points.erase(Points.begin() + centrePoint.position);
    }
}

DataPoint MinPoint(DataPoint p1, DataPoint p2) {
    if (p1.x < p2.x || p1.y < p2.y){
        return p1;
    }

    return p2;
}

DataPoint MaxPoint(DataPoint p1, DataPoint p2) {
    if (p1.x > p2.x || p1.y > p2.y){
        return p1;
    }

    return p2;
}

bool setupO = false;
int oldPoints = 0;

void Calculate() {
    int processId = GetProcessId();
    numberOfPoints = (int) Points.size();

    if (setupO == false || numberOfPoints != oldPoints) {
        global[0] = (size_t) numberOfPoints;

        setup_openCL_device_context_queue_kernel((char*) "./kmeans.cl" , (char*) "calc");
        setupO = true;
        oldPoints = numberOfPoints;
    }

    setup_kernel_memory(numberOfPoints);

    copy_kernel_args();

    //submit the kernel for execution 
    clEnqueueNDRangeKernel(queue, kernel, 1, NULL, global, local, 0, NULL, &event);
    clWaitForEvents(1, &event);

    //copying data from the device back to host c matrix
    clEnqueueReadBuffer(queue, bufA, CL_TRUE, 0, SIZE * SIZE * sizeof(DataPoint), &MATRIX[0], 0, NULL, NULL);

    //frees memory for device, kernel, queue, etc.
    //you will need to modify this to free your own buffers
    free_memory();
}

void MergeMatrix(DataPoint Sub[SIZE * SIZE]) {
    for (int i = 0; i < SIZE; i++) {
        for (int z = 0; z < SIZE; z++) {
            if (Sub[i * SIZE + z].cls > 0 && MATRIX[i * SIZE + z].cls < 1) {
                MATRIX[i * SIZE + z].cls = Sub[i * SIZE + z].cls;
            }
        }
    }
}

int GetBlockSize(){
    return (Points.size() / GetProcessesCount()) - 1; //Calculate chunk size based on process count
}

int GetBlockStart(int processId){
    const int blockSize = GetBlockSize();
    return processId > 0 ? processId * blockSize + processId : processId * blockSize; //Calculate start of block based on process id
}

int GetBlockEnd(int processId){
    const int blockStart = GetBlockStart(processId);
    const int blockSize = GetBlockSize();
    return blockStart + blockSize > Points.size() ? Points.size() - 1 : blockStart + blockSize; //Calculate end of block based on process id
}

void HeadNode() {
    //Populate Array
    PopulateMatrix();

    int processId = GetProcessId();
    int processCount = GetProcessesCount();
    int chunkSize = GetBlockSize();

    printf("Process (%d): Process Count=%d | Points Count=%d | Chunk Size=%d | Bytes=%d\n", processId, processCount, (int) Points.size(), chunkSize, (int) sizeof(MATRIX));

    clock_gettime(CLOCK_MONOTONIC, &Start);

    for (int iterations = 0; iterations < MAX_ITERATIONS; iterations++) {
        //printf("--------------------- ITERATION %d ---------------------\n", iterations);
        SelectK();

        MPIBroadcast(&MATRIX[0], SIZE * SIZE, MPI_DATAPOINT, 0, MPI_COMM_WORLD); //Broadcast matrix
        MPIBroadcast(&KPoints[0], K, MPI_DATAPOINT, 0, MPI_COMM_WORLD); //Broadcast k values

        //Send chunks from points vector
        for (int i = 0; i < processCount; i++) {
            if (i == 0) continue;

            int chunkStart = GetBlockStart(i);
            int chunkEnd = GetBlockEnd(i);
            int size = abs(chunkStart - chunkEnd) + 1;

            vector<DataPoint> SubPoints(Points.begin() + chunkStart, (Points.begin() + chunkEnd + 1));

            printf("ITER[%d] Process (%d): Start=%d | End=%d | Size=%d\n", iterations, processId, chunkStart, chunkEnd, (int) SubPoints.size());

            size = SubPoints.size();
            MPISend(&size, 1, MPI_INT, i, MPI_COMM_WORLD);
            MPISend(&SubPoints[0], size, MPI_DATAPOINT, i, MPI_COMM_WORLD);
        }

        int chunkEnd = GetBlockEnd(0);
        vector<DataPoint> tempPoints(Points.begin(), Points.end());
        Points.erase(Points.begin() + chunkEnd + 1, Points.end());
        Calculate();
        Points = tempPoints;

        //Wait for chunks from points vector
        for (int i = 0; i < processCount; i++) {
            if (i == 0) continue;

            MPIRecieve(&SubMatrix[0], SIZE * SIZE, MPI_DATAPOINT, i, MPI_COMM_WORLD);
            MergeMatrix(SubMatrix);
            //printf("Process (%d): Recieved Matrix from process %d\n", 0, i);
        }

        ITER++;
    }

    clock_gettime(CLOCK_MONOTONIC, &End);
    print_timediff("Elapsed Time: ~", Start, End);

    PrintMatrix("Final Result");
}

void SlaveRoutine() {
    int processId = GetProcessId();

    for (int iterations = 0; iterations < MAX_ITERATIONS; iterations++) {
        MPIBroadcast(&MATRIX[0], SIZE * SIZE, MPI_DATAPOINT, 0, MPI_COMM_WORLD); //Broadcast matrix
        MPIBroadcast(&KPoints[0], K, MPI_DATAPOINT, 0, MPI_COMM_WORLD); //Broadcast k values
        //printf("Process (%d): Recieved Matrix and inital k values\n", processId);

        //Get size of incoming vector
        int pointsSize;
        MPIRecieve(&pointsSize, 1, MPI_INT, 0, MPI_COMM_WORLD);
        //printf("Process (%d): Recieved points count = %d\n", processId, pointsSize);

        Points.resize(pointsSize);
        MPIRecieve(&Points[0], pointsSize, MPI_DATAPOINT, 0, MPI_COMM_WORLD);
        //printf("Process (%d): Recieved points array = %d\n", processId, (int) Points.size());

        Calculate();
        //printf("Process (%d): Calculated\n", processId);

        MPISend(&MATRIX[0], SIZE * SIZE, MPI_DATAPOINT, 0, MPI_COMM_WORLD);
        //printf("Process (%d): Done and sent...\n", processId);
    }
}

int main() {
    InitializeMPI();

    int processId = GetProcessId(); 

    //Create custom mpi data type
    const int nitems = 3;
    int blocklengths[nitems] = {1, 1, 1};
    MPI_Datatype types[nitems] = {MPI_INT, MPI_INT, MPI_INT};
    MPI_Aint offsets[nitems];

    offsets[0] = offsetof(DataPoint, x);
    offsets[1] = offsetof(DataPoint, y);
    offsets[2] = offsetof(DataPoint, cls);

    MPI_Type_create_struct(nitems, blocklengths, offsets, types, &MPI_DATAPOINT);
    MPI_Type_commit(&MPI_DATAPOINT);
    //printf("Process (%d) has comitted DataPoint struct....\n", processId);

    if (processId == 0) {
        HeadNode();
    }
    else {
        SlaveRoutine();
    }

    MPI_Type_free(&MPI_DATAPOINT);
    FinalizeMPI();
}