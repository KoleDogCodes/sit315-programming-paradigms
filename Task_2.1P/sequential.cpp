#include <iostream>
#include <stdlib.h>
#include <ctime>
#include <fstream>

using namespace std;

#define ROWS 10
#define COLUMNS 5
#define MAX_NUMBER 10

//NOTE: ARRAY[ROW][COLUMN]

long A[ROWS][COLUMNS];
long B[COLUMNS][ROWS];

void PopulateMatrixes() {
    srand(0);
    //Populate array a
    for (int i = 0; i < ROWS; i++) {
        for (int z = 0; z < COLUMNS; z++) {
            A[i][z] = rand() % MAX_NUMBER;
        }
    }

    //Populate array b
    for (int i = 0; i < COLUMNS; i++) {
        for (int z = 0; z < ROWS; z++) {
            B[i][z] = rand() % MAX_NUMBER;
        }
    }
}

void PrintMatrix(long *matrix, int m, int n) {
    ofstream myfile;
    myfile.open ("task 1 results.txt");

    for (int i = 0; i < m; i++) {
        for (int z = 0; z < n; z++) {
            myfile << *((matrix+i*n) + z);
            myfile << ",";
            
        }

        myfile << endl;
    }

    myfile.close();
}

void print_timediff(const char* prefix, const struct timespec& start, const struct timespec& end) {
    double milliseconds = end.tv_nsec >= start.tv_nsec
                        ? (end.tv_nsec - start.tv_nsec) / 1e6 + (end.tv_sec - start.tv_sec) * 1e3
                        : (start.tv_nsec - end.tv_nsec) / 1e6 + (end.tv_sec - start.tv_sec - 1) * 1e3;
    printf("%s: %lf seconds\n", prefix, (milliseconds / 1000));
}

void Calculate(){
    long C[ROWS][ROWS];
    struct timespec start, end;

    clock_gettime(CLOCK_MONOTONIC, &start);
    for (int i = 0; i < ROWS; i++) {
        for (int z = 0; z < ROWS; z++) {
            //Now calculate 
            long res = 0;

            for (int iA = 0; iA < COLUMNS; iA++) {
                res += A[i][iA] * B[iA][z];
            }

            C[i][z] = res;
        }    
    }
    clock_gettime(CLOCK_MONOTONIC, &end);    

    print_timediff("Matrix C: ", start, end);
    PrintMatrix(*C, ROWS, ROWS);
}

int main() {
    //Populate matrices with random values
    PopulateMatrixes();

    //Calulate Matrix
    Calculate();

    return 0;
}