#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <ctime>
#include <vector>
#include <fstream>

using namespace std;

//Constants
#define ROWS 1000
#define COLUMNS 1000
#define MAX_NUMBER 100
volatile int CURRENT_ROW = 0;

//Thread Variables
struct ThreadArgs {
    int row;
    long id;
};


//Arrays
//NOTE: ARRAY[ROW][COLUMN]
long A[ROWS][COLUMNS];
long B[COLUMNS][ROWS];
long C[ROWS][ROWS];

void PopulateMatrixes() {
    //Populate array a
    for (int i = 0; i < ROWS; i++) {
        for (int z = 0; z < COLUMNS; z++) {
            A[i][z] = rand() % MAX_NUMBER;
        }
    }

    //Populate array b
    for (int i = 0; i < COLUMNS; i++) {
        for (int z = 0; z < ROWS; z++) {
            B[i][z] = rand() % MAX_NUMBER;
        }
    }
}

void PrintMatrix() {
    //Open File to dump results
    ofstream myfile;
    myfile.open ("task 1 results.txt");

    for (int i = 0; i < ROWS; i++) {
        for (int z = 0; z < ROWS; z++) {

            //Write current value at MATRIX[i][z] location
            myfile << C[i][z];
            myfile << ",";
            
        }

        myfile << endl;
    }

    myfile.close();
}

void print_timediff(const char* prefix, const struct timespec& start, const struct timespec& end) {
    //Calculate time difference in milliseconds
    double milliseconds = end.tv_nsec >= start.tv_nsec
                        ? (end.tv_nsec - start.tv_nsec) / 1e6 + (end.tv_sec - start.tv_sec) * 1e3
                        : (start.tv_nsec - end.tv_nsec) / 1e6 + (end.tv_sec - start.tv_sec - 1) * 1e3;
    printf("%s: %lf seconds\n", prefix, (milliseconds / 1000));
}

void CalculateRow(int i) {
    /*
     * Loop thorugh each row in resultant Matrix C
    */
    for (int z = 0; z < ROWS; z++) {
        long res = 0;

        //Loop thorugh the row in Matrix A at row i and the column in Matrix B at column z
        for (int iA = 0; iA < COLUMNS; iA++) {
            res += A[i][iA] * B[iA][z];
        }

        C[i][z] = res;
    }
}

void* SubRoutine(ThreadArgs args) {

    #pragma omp task 
    {
        CalculateRow(args.row);

        //Recuriosn validation to check if any more rows need to be calculated
        if (CURRENT_ROW < ROWS) {
            //Recursevly call itself with the next row to solve
            int new_row = CURRENT_ROW++;
            args.row = new_row;
            SubRoutine(args);
        }

    }
}

int main() {
    //Populate matrices with random values
    PopulateMatrixes();

    //Calulate Matrix
    struct timespec start, end;
    clock_gettime(CLOCK_MONOTONIC, &start);

    #pragma omp parallel num_threads(10)
    {
        ThreadArgs args;
        args.row = CURRENT_ROW++;
        args.id = 0;
        SubRoutine(args);
    }
    
    clock_gettime(CLOCK_MONOTONIC, &end);  

    print_timediff("Solved Matrix C in ", start, end);

    //Output Matrix
    PrintMatrix();
    return 0;
}