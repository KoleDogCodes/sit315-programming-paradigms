#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <pthread.h> 
#include "print_utili.h"
#include "mpi_wrapper.h"

using namespace std;

int PRODUCER_THREADS;
int CONSUMER_THREADS;
int QUEUE_SIZE;
 
struct TrafficSignal {
    int trafficId;
    int carsPassed;
    int timestamp;
};

struct ThreadArgs {
    int threadId;
    int lineId;
};

//Variables
vector<TrafficSignal> signals;
vector<TrafficSignal> signalQueue;
vector<TrafficSignal> results;
int CURRENT_ROW = 0;

//Thread Variables
vector<pthread_t> producerThreads;
vector<pthread_t> consumerThreads;
pthread_mutex_t accessLock;
MPI_Datatype MPI_DATASIGNAL;

vector<string> StringSplit(string phrase, string delimiter){
    vector<string> list;
    string s = string(phrase);
    size_t pos = 0;
    string token;
    while ((pos = s.find(delimiter)) != string::npos) {
        token = s.substr(0, pos);
        list.push_back(token);
        s.erase(0, pos + delimiter.length());
    }
    list.push_back(s);
    return list;
}

void LoadData() {
    ifstream myfile;
    myfile.open("sample_data.txt");

    string line;

    while (getline(myfile, line)) {
        //Spilt line from TRAFFIC ID,CARS PASSED,TIMESTAMP
        vector<string> spilts = StringSplit(line, ",");
            
        TrafficSignal sig = TrafficSignal();
        sig.trafficId = atoi(spilts[0].c_str());
        sig.carsPassed = atoi(spilts[1].c_str());
        sig.timestamp = atoi(spilts[2].c_str());

        signals.push_back(sig);
    }

    //Reverse array 
    reverse(signals.begin(), signals.end());

    CURRENT_ROW = results.size() - 1;

    SmartPrint("[MAIN] " + to_string(signals.size()) + " signals has been loaded.");
}

bool compareSignals(TrafficSignal s1, TrafficSignal s2) { 
    return (s1.carsPassed > s2.carsPassed); 
}

void* ProducerRoutine(void *input) {
    //Cast void pointer to correct data type
    ThreadArgs* args = (struct ThreadArgs*) input;

    while (true) {
        //Exit thread if no more signals to process
        if (signals.size() == 0) {
            pthread_exit(NULL);
        }

        //Lock all producer threads
        pthread_mutex_lock(&accessLock); 

        //Validation for recursive purposes
        if (!signals.empty() && signalQueue.size() < QUEUE_SIZE) {
            TrafficSignal &sig = signals[signals.size() - 1];
            signals.pop_back();

            //Add traffic signal to the queue
            signalQueue.push_back(sig);
            args->lineId = CURRENT_ROW++;
        }

        //Unlock producer threads
        pthread_mutex_unlock(&accessLock);
    }
}

void* ConsumerRoutine(void *input) {
    //Cast void pointer to correct data type
    ThreadArgs* args = (struct ThreadArgs*) input;

    while (true) {
        //Exit thread if zero signals and queue is empty
        if (signals.size() == 0 && signalQueue.size() == 0){
            pthread_exit(NULL);
        }

        //Lock the consumer threads
        pthread_mutex_lock(&accessLock);

        if (signalQueue.size() > 0){
            TrafficSignal sig = signalQueue[0];
            signalQueue.erase(signalQueue.begin());

            bool exists = false;

            //Find the corrosponding traffic id from array and update it
            for (int i = 0; i < results.size(); i++) {
                if (sig.trafficId == results[i].trafficId){
                    results[i].carsPassed += sig.carsPassed;
                    exists = true;
                }
            }
            
            //Add traffic signal to array if it does not exist
            if (exists == false) {
                results.push_back(sig);
            }

            //Sort array
            sort(results.begin(), results.end(), compareSignals);
        }

        //Unlock the consumer threads
        pthread_mutex_unlock(&accessLock);
    }
}

void StartProducers() {
    //Initlaise thread array
    for (int i = 0; i < PRODUCER_THREADS; i++) {
        pthread_t t;
        producerThreads.push_back(t);
    }

    //Starting threads
    for (int i = 0; i < PRODUCER_THREADS; i++) {
        ThreadArgs *args = (struct ThreadArgs *) malloc(sizeof(struct ThreadArgs));
        args->threadId = i;
        args->lineId = CURRENT_ROW++;

        pthread_create(&producerThreads[i], NULL, ProducerRoutine, (void *) args); 
    }
}

void StartConsumers() {
    //Initlaise thread array
    for (int i = 0; i < CONSUMER_THREADS; i++) {
        pthread_t t;
        consumerThreads.push_back(t);
    }

    //Starting threads
    for (int i = 0; i < CONSUMER_THREADS; i++) {
        ThreadArgs *args = (struct ThreadArgs *) malloc(sizeof(struct ThreadArgs));
        args->threadId = i;
        args->lineId = 0;

        pthread_create(&consumerThreads[i], NULL, ConsumerRoutine, (void *) args); 
    }
}

void Solve() {
    //Initilaise mutex
    if (pthread_mutex_init(&accessLock, NULL) != 0) { 
        printf("\n[MAIN]: Access Mutex lock init has failed.\n"); 
        return; 
    } 

    //Start producer threads
    StartConsumers();
    StartProducers();
    
    for (int i = 0; i < PRODUCER_THREADS; i++){
        pthread_join(producerThreads[i], NULL);
    }

    for (int i = 0; i < CONSUMER_THREADS; i++){
        pthread_join(consumerThreads[i], NULL);
    }

    //Destory mutex operations
    pthread_mutex_destroy(&accessLock);
}

int GetChunkSize(int hours){
    return (12 * hours); //Calculate how many signals for X hrs
}

void HeadNode() {
    LoadData(); //Load file into memory

    int processCount = GetProcessesCount();
    const int chunkSize = GetChunkSize(24);
    const int chunksCount = signals.size() / chunkSize;
    int chunkId = 0;
    int complete = 0;

    //printf("%d chunks to process...\n", chunksCount);

    //Send Chunks
    while (chunkId < chunksCount) {

        for (int pid = 0; pid < processCount; pid++) {
            if (pid == 0) continue; 

            int chunkStart = (chunkId * chunkSize);
            int chunkEnd = chunkStart + chunkSize > signals.size() ? signals.size() : chunkStart + chunkSize;

            MPIBroadcast(&complete, 1, MPI_INT, 0, MPI_COMM_WORLD); //Send current compeltion status

            vector<TrafficSignal> signalsChunk(signals.begin() + chunkStart, signals.begin() + chunkEnd);

            //printf("Day(%d) Chunk Start=%d | Chunk End=%d | Chunk Size=%d\n", chunkId + 1, chunkStart, chunkEnd, (int)signalsChunk.size());

            int size = signalsChunk.size();
            MPISend(&size, 1, MPI_INT, pid, MPI_COMM_WORLD); //Send size of chunk of signals

            MPISend(&chunkId, 1, MPI_INT, pid, MPI_COMM_WORLD); //Send id of current chunk

            MPISend(&signalsChunk[0], size, MPI_DATASIGNAL, pid, MPI_COMM_WORLD); //Send signal data
            chunkId += 1;
        }
    }

    // complete = 1;
    // MPIBroadcast(&complete, 1, MPI_INT, 0, MPI_COMM_WORLD);

    //Start listener thread
    chunkId = 0;
    while (true) {
        for (int pid = 0; pid < processCount; pid++) {
            if (pid == 0) continue; 

            int size;
            MPIRecieve(&size, 1, MPI_INT, pid, MPI_COMM_WORLD); //Send size of chunk of signals

            int subChunkId;
            MPIRecieve(&subChunkId, 1, MPI_INT, pid, MPI_COMM_WORLD); //Send id of current chunk

            results.clear();
            results.resize(size);
            MPIRecieve(&results[0], size, MPI_DATASIGNAL, pid, MPI_COMM_WORLD); //Send signal data
            chunkId += 1;

            //Print top 3 congested lights
            cout << "----------------------- DAY " << chunkId << "-----------------------" << endl;
            cout << "Traffic ID: " + to_string(results[0].trafficId) + " | Number: " + to_string(results[0].carsPassed) << endl;
            cout << "Traffic ID: " + to_string(results[1].trafficId) + " | Number: " + to_string(results[1].carsPassed) << endl;
            cout << "Traffic ID: " + to_string(results[2].trafficId) + " | Number: " + to_string(results[2].carsPassed) << endl << endl;
        }

        if (chunkId >= chunksCount) break;
    }
}

void SlaveNode() {
    int processId = GetProcessId();
    int complete;
    int globalChunkId = 0;

    while (true) {
        MPIBroadcast(&complete, 1, MPI_INT, 0, MPI_COMM_WORLD);
        //printf("Process (%d) Completion status is: %d\n", processId, complete);

        if (complete == 1) {
            printf("Process (%d) is done...", processId);
            break;
        }

        //Recieve size of signals
        int size;
        MPIRecieve(&size, 1, MPI_INT, 0, MPI_COMM_WORLD);
        //printf("Process (%d) has incoming data of size %d\n", processId, size);

        int chunkId;
        MPIRecieve(&chunkId, 1, MPI_INT, 0, MPI_COMM_WORLD);

        //Buffer signals into array
        signals.clear();
        signals.resize(size);
        results.clear();
        results.resize(0);

        MPIRecieve(&signals[0], size, MPI_DATASIGNAL, 0, MPI_COMM_WORLD);

        Solve();

        sort(results.begin(), results.end(), compareSignals);

        //Send size of results array
        size = results.size();
        MPISend(&size, 1, MPI_INT, 0, MPI_COMM_WORLD);

        //Send chunk id
        MPISend(&chunkId, 1, MPI_INT, 0, MPI_COMM_WORLD);

        //Send results array
        MPISend(&results[0], size, MPI_DATASIGNAL, 0, MPI_COMM_WORLD);
    }
}

int main() {
    //Initlaise simulation paramaters
    srand(time(NULL));
    PRODUCER_THREADS = (rand() % 8) + 2;
    CONSUMER_THREADS = (rand() % 8) + 2;
    QUEUE_SIZE = (rand() % 20) + 10;

    //Initlaise MPI
    InitializeMPI();

    int processId = GetProcessId();

    //Create custom mpi data type
    const int nitems = 3;
    int blocklengths[nitems] = {1, 1, 1};
    MPI_Datatype types[nitems] = {MPI_INT, MPI_INT, MPI_INT};
    MPI_Aint offsets[nitems];

    offsets[0] = offsetof(TrafficSignal, trafficId);
    offsets[1] = offsetof(TrafficSignal, carsPassed);
    offsets[2] = offsetof(TrafficSignal, timestamp);

    MPI_Type_create_struct(nitems, blocklengths, offsets, types, &MPI_DATASIGNAL);
    MPI_Type_commit(&MPI_DATASIGNAL);

    if (processId == 0) {
        HeadNode();
    }
    else {
        SlaveNode();
    }

    if (processId == 0) {
        //Print simulation parameters
        cout << endl << "------------ Simulator Parameters (PARRALLEL) ------------" << endl;
        cout << "Producer Thread Count: " << PRODUCER_THREADS << endl;
        cout << "Consumer Thread Count: " << CONSUMER_THREADS << endl;
        cout << "Queue Size Count: " << QUEUE_SIZE << endl;
    }

    MPI_Type_free(&MPI_DATASIGNAL);
    FinalizeMPI();

    return 1;
}